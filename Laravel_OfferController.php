<?php

class OfferController extends BaseController {

   
    public function __construct() {
        if (!Auth::check()) {
            $this->beforeFilter('auth');
        }
        parent::__construct();
    }

    //offers
    public function get_list($page = 1, $my = false) {
        if (Input::has("page")) {
            $new_page = array_keys(Input::get("page"))[0];
            if ($new_page == "next") {
                $page++;
            } elseif ($new_page == "prev") {
                $page--;
            } else {
                $page = $new_page;
            }
        }

        $user = User::with("offers")->get()->find(Auth::user()->id);

        $cats = array();
        if (Input::has("cat")) {
            $cats = array_keys(Input::get("cat"));
        }
        if (Input::has("mobile")) {
            $cats[] = 102;
        }

        if ($my) {
            $user = User::with(array('offers' => function($query) use ($page, $cats) {
                    $query->where("is_blocked", 0)->wherePivot("blocked", "0");
                    if (Input::has("search")) {
                        $query->where(function ($queryy) {
                            $queryy->where("name", "like", "%" . Input::get("search") . "%")
                                    ->orWhere("description", "like", "%" . Input::get("search") . "%");
                        });
                    }
                    if (Input::has("deeplink")) {
                        $query->where("deeplink", 1);
                    }
                    if (!empty($cats)) {
                        $query->whereHas('categories', function($q) use ($cats) {
                            $q->whereRaw('category_id in (' . implode(", ", $cats) . ')');
                        });
                    }
                }))->get()->find(Auth::user()->id);

            $offers_count = ceil(count($user->offers) / 20);
            if (Input::has("export"))
                $offers = $user->offers;
            else
                $offers = $user->offers->slice(($page - 1) * 20, 20);
            //print_r($offers);
        } else {
            $offers = Offer::where("is_blocked", "0");
            if (Input::has("search")) {
                $offers->where(function ($query) {
                    $query->where("name", "like", "%" . Input::get("search") . "%")
                            ->orWhere("description", "like", "%" . Input::get("search") . "%");
                });
            }
            if (Input::has("deeplink")) {
                $offers->where("deeplink", 1);
            }
            if (!empty($cats)) {
                $offers->whereHas('categories', function($q) use ($cats) {
                    $q->whereRaw('category_id in (' . implode(", ", $cats) . ')');
                });
            }
            $offers_count = ceil($offers->count() / 20);
            if (Input::has("export"))
                $offers = $offers->get();
            else
                $offers = $offers->take(20)->skip(($page - 1) * 20)->get();
        }

        $cats = array();
        //in the filter to show only those whose offers have not blocked
        $ofs = Offer::where("is_blocked", 0)->with(array('categories' => function($query) {
                $query->groupBy("category_id");
            }))->get();
        $blocked = array();
        foreach ($user->offers()->wherePivot("blocked", 1)->get() as $o) {
            $blocked[] = $o->id;
        }
        foreach ($ofs as $o) {
            if (!in_array($o->id, $blocked))
                foreach ($o->categories as $v) {
                    //print $v->id."<br>";
                    $cats[] = $v;
                }
        }
        
        if (Input::has("export")) {
            $table = [];
            $j = 2;
            foreach ($offers as $item) {
                $table[] = [
                    "cell_x" => 0,
                    "cell_y" => $j,
                    "value" => $item->name
                ];
                $table[] = [
                    "cell_x" => 1,
                    "cell_y" => $j,
                    "value" => $item->price
                ];
                $table[] = [
                    "cell_x" => 2,
                    "cell_y" => $j,
                    "value" => $item->epc
                ];
                $table[] = [
                    "cell_x" => 3,
                    "cell_y" => $j,
                    "value" => $item->rating
                ];
                $table[] = [
                    "cell_x" => 4,
                    "cell_y" => $j,
                    "value" => $item->allocation($user->id)." р ".$item->geoString
                ];
                $j++;
            }
            return $this->report($table);
        }
        
        return View::make("new_inner.offers", array(
                    "offers" => $offers,
                    "user" => $user,
                    "cats" => $cats,
                    "count" => $offers_count,
                    "page" => $page,
                    "my" => $my,
                    "show_filter" => array("url" => "offers/1/" . $my, "type" => "offers"),
        ));
    }

    function report($table) {
        // Connected class to work with excel
        require_once(public_path().'/PHPExcel/Classes/PHPExcel.php');
        // Connected class to output data in excel format
        require_once(public_path().'/PHPExcel/Classes/PHPExcel/Writer/Excel5.php');

        // Creating object of PHPExcel class
        $xls = new PHPExcel();
        // Install index of active sheet
        $xls->setActiveSheetIndex(0);
        // Receive active sheet
        $sheet = $xls->getActiveSheet();
        // Sigin sheet
        $sheet->setTitle("Офферы");

//        $sheet->setCellValue("A1", 'ЛОГО');
        $sheet->setCellValue("A1", 'ОФФЕР');
        $sheet->setCellValue("B1", 'СТАВКИ');
        $sheet->setCellValue("C1", 'EPC');
        $sheet->setCellValue("D1", 'РЕЙТИНГ');
        $sheet->setCellValue("E1", 'ОТЧИСЛЕНИЕ И ГЕО');

        $sheet->getStyle("A1:E1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('58585a');
        $sheet->getStyle("A1:E1")->getFont()->getColor()->setRGB('FFFFFF');

        $sheet->getStyle("A1:E1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getRowDimension(1)->setRowHeight(25);

        foreach ($table as $cell) {
            $sheet->setCellValueByColumnAndRow(
                    $cell["cell_x"], $cell["cell_y"], $cell["value"]);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        
        // Output HTTP-headers
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=export.xls");

        // Output file contents
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }
    
    //add to our
    public function getAddOffer($id) {
        $offer = Offer::find($id);
        if (!$offer)
            return View::make("new_inner.not_found");
        if ($offer->levelup && count(Auth::user()->payouts) == 0) {
            $user = User::with("offers")->get()->find(Auth::user()->id);
            return View::make("new_inner.offer", array(
                        "item" => $offer,
                        "user" => $user,
                        "show_offer" => true,
                        "level_error" => true,
            ));
        }
        //check offer in my favorites
        $offers = unserialize(file_get_contents("https://api.actionpay.ru/ru/apiWmMyOffers/?key=Y0Fj1Z6gZyTBkeGa&format=php&offer=" . $id));
        //если нет, то добавляем
        if (isset($offers["error"]) && $offers["error"]["code"] == 403) {
            $add_offer = unserialize(file_get_contents("https://api.actionpay.ru/ru/apiWmMyOffers/?key=Y0Fj1Z6gZyTBkeGa&format=php&act=add&offer=" . $id));
        }
        //notted our to user
        $user = Auth::user();
        if (!$user->offers()->find($id))
            $user->offers()->attach($id);
        if (Request::ajax())
            print asset('offer/show/' . $offer->id);
        else 
            return Redirect::back();
    }

    //Remove from our
    public function getRemoveOffer($id) {
        $offer = Offer::find($id);
        if (!$offer)
            return View::make("new_inner.not_found");
        //Notted in our to user
        $user = Auth::user();
        $user->offers()->detach($id);
        return Redirect::back();
    }

    //one offer
    public function getShow($id) {
        $item = Offer::find($id);
        if (!$item)
            return View::make("new_inner.not_found");
        $user = User::with("offers")->get()->find(Auth::user()->id);
        return View::make("new_inner.offer", array(
                    "item" => $item,
                    "user" => $user,
                    "show_offer" => true,
        ));
    }

    //all stream this offer
    public function getStreams($id) {
        $item = Offer::find($id);
        if (!$item)
            return View::make("new_inner.not_found");
        $user = Auth::user();
        $streams = $user->streams()->where("offer_id", $id)->get();
        return View::make("new_inner.stream", array(
                    "list" => $streams,
                    "item" => $item,
                    "user" => $user,
                    "show_offer" => true,
        ));
    }

    //creating stream
    public function getCreateStream($id) {
        $item = Offer::find($id);
        if (!$item)
            return View::make("new_inner.not_found");
        $user = Auth::user();
        $domains = array("");
        foreach ($user->domains as $el) {
            $domains[$el->id] = $el->domain;
        }
        $accounts = array(
            "" => "",
        );
        foreach ($user->accounts as $el) {
            $accounts[$el] = $el;
        }
        return View::make("new_inner.stream", array(
                    "item" => $item,
                    "user" => $user,
                    "domains" => $domains,
                    "accounts" => $accounts,
                    "show_offer" => true,
        ));
    }

    public function postCreateStream($id) {
        $item = Offer::find($id);
        $user = Auth::user();
        $valid = array(
            "stream_name" => "required|max:255",
            "landing" => "required",
        );
        $validation = Validator::make(Input::all(), $valid);
        if ($validation->fails()) {
            return Redirect::back()->withInput()->withErrors($validation);
        }
        $data = array(
            "name" => Input::get("stream_name"),
            "user_id" => $user->id,
            "offer_id" => $id,
            "landing_id" => Input::get("landing"),
            "subaccount" => "",
            "domain_id" => Input::get("domains"),
        );
        if (Input::get("accounts") != "") {
            $data["subaccount"] = Input::get("accounts");
        } elseif (Input::has("account_name")) {
            $data["subaccount"] = Input::get("account_name");
        }
        Stream::create($data);
        return Redirect::to("/offer/streams/" . $id);
    }

    //editing stream
    public function getEditStream($id) {
        $stream = Stream::find($id);
        if (!$stream)
            return View::make("new_inner.not_found");
        $id = $stream->offer_id;
        $item = Offer::find($id);
        if (!$item)
            return View::make("new_inner.not_found");
        $user = Auth::user();
        $domains = array("");
        foreach ($user->domains as $el) {
            $domains[$el->id] = $el->domain;
        }
        $accounts = array(
            "" => "",
        );
        foreach ($user->accounts as $el) {
            $accounts[$el] = $el;
        }
        return View::make("new_inner.stream", array(
                    "stream" => $stream,
                    "item" => $item,
                    "user" => $user,
                    "domains" => $domains,
                    "accounts" => $accounts,
                    "show_offer" => true,
        ));
    }

    public function postEditStream($id) {
        $stream = Stream::find($id);
        if (!$stream)
            return View::make("new_inner.not_found");
        $valid = array(
            "stream_name" => "required|max:255",
            "landing" => "required",
        );
        $validation = Validator::make(Input::all(), $valid);
        if ($validation->fails()) {
            return Redirect::back()->withInput()->withErrors($validation);
        }
        $stream->name = Input::get("stream_name");
        $stream->landing_id = Input::get("landing");
        $stream->subaccount = "";
        $stream->domain_id = Input::get("domains");
        if (Input::get("accounts") != "") {
            $stream->subaccount = Input::get("accounts");
        } elseif (Input::has("account_name")) {
            $stream->subaccount = Input::get("account_name");
        }
        $stream->save();
        return Redirect::to("/offer/streams/" . $stream->offer_id);
    }

    //removing stream (ajax)
    public function postDeleteStream($id) {
        $stream = Stream::find($id);
        if (!$stream)
            return View::make("new_inner.not_found");

        //removing clicks
        Click::where("stream_id", $stream->id)->delete();

        $stream->delete();
        print "success";
        return;
    }

    /*     * *** ADMIN ***** */

    public function offers($mode = "active", $page = 1) {
        if (Input::has("page")) {
            $new_page = array_keys(Input::get("page"))[0];
            if ($new_page == "next") {
                $page++;
            } elseif ($new_page == "prev") {
                $page--;
            } else {
                $page = $new_page;
            }
        }

        $offers = Offer::where("is_blocked", ($mode == "active" ? "0" : "1"));
        if (Input::has("search")) {
            $offers->where(function ($query) {
                $query->where("name", "like", "%" . Input::get("search") . "%")
                        ->orWhere("description", "like", "%" . Input::get("search") . "%");
            });
        }
        if (Input::has("deeplink")) {
            $offers->where("deeplink", 1);
        }
        $cats = array();
        if (Input::has("cat")) {
            $cats = array_keys(Input::get("cat"));
        }
        if (Input::has("mobile")) {
            $cats[] = 102;
        }
        if (!empty($cats)) {
            $offers->whereHas('categories', function($q) use ($cats) {
                $q->whereRaw('category_id in (' . implode(", ", $cats) . ')');
            });
        }

        $offers_count = ceil($offers->count() / 20);
        if (Input::has("export"))
            $offers = $offers->get();
        else
            $offers = $offers->take(20)->skip(($page - 1) * 20)->get();

        $cats = Category::all();

        $res = array();
        $ips = array();
        $empty = array(
            "clicks" => 0,
            "uniques" => 0,
            "cr" => 0,
            "epc" => 0,
            "conv_all" => 0,
            "conv_accept" => 0,
            "conv_proceed" => 0,
            "conv_reject" => 0,
            "fin_all" => 0,
            "fin_accept" => 0,
            "fin_proceed" => 0,
            "fin_reject" => 0,
        );

        //for each users
        foreach ($offers as $offer) {
            $item = $empty;
            $item["offer_id"] = $offer->id;
            $item["offer_name"] = $offer->name;
            $item["offer"] = $offer;
            //receiving click by them
            foreach (Click::where("offer_id", $offer->id)->get() as $click) {
                $item["clicks"] ++;
                if (!isset($ips[$click->offer_id]) || !isset($ips[$click->offer_id][$click->stream_id]) || !isset($ips[$click->offer_id][$click->stream_id][$click->user_id]) || !in_array($click->ip, $ips[$click->offer_id][$click->stream_id][$click->user_id])) {
                    $item["uniques"] ++;
                    $ips[$click->offer_id][$click->stream_id][$click->user_id][] = $click->ip;
                }
            }

            //receiving statistic by them
            $stat = Stat::where("offer_id", $offer->id)->get();
            foreach ($stat as $el) {
                $item["cr"] += $item["uniques"] == 0 ? 0 : $el->accepted / $item["uniques"];
                $item["epc"] += $el->epc;
                $item["conv_all"] += $el->conversion;
                $item["conv_accept"] += $el->accepted;
                $item["conv_proceed"] += $el->processing;
                $item["conv_reject"] += $el->rejected;
                $item["fin_all"] += ($el->hold + $el->paid);
                $item["fin_accept"] += $el->paid;
                $item["fin_proceed"] += $el->hold;
            }
            //writting
            $res[$offer->name] = $item;
        }
        
        if (Input::has("export")) {
            $j = 3;
            $table = [];
            foreach ($res as $offer_name => $value) {
                $table[] = [
                    "cell_x" => 0,
                    "cell_y" => $j,
                    "value" => $offer_name
                ];
                $table[] = [
                    "cell_x" => 1,
                    "cell_y" => $j,
                    "value" => $value["clicks"]
                ];
                $table[] = [
                    "cell_x" => 2,
                    "cell_y" => $j,
                    "value" => $value["uniques"]
                ];
                $table[] = [
                    "cell_x" => 3,
                    "cell_y" => $j,
                    "value" => $value["cr"]
                ];
                $table[] = [
                    "cell_x" => 4,
                    "cell_y" => $j,
                    "value" => $value["epc"]
                ];
                $table[] = [
                    "cell_x" => 5,
                    "cell_y" => $j,
                    "value" => $value["conv_all"]
                ];
                $table[] = [
                    "cell_x" => 6,
                    "cell_y" => $j,
                    "value" => $value["conv_accept"]
                ];
                $table[] = [
                    "cell_x" => 7,
                    "cell_y" => $j,
                    "value" => $value["conv_proceed"]
                ];
                $table[] = [
                    "cell_x" => 8,
                    "cell_y" => $j,
                    "value" => $value["conv_reject"]
                ];
                $table[] = [
                    "cell_x" => 9,
                    "cell_y" => $j,
                    "value" => $value["fin_all"]
                ];
                $table[] = [
                    "cell_x" => 10,
                    "cell_y" => $j,
                    "value" => $value["fin_accept"]
                ];
                $table[] = [
                    "cell_x" => 11,
                    "cell_y" => $j,
                    "value" => $value["fin_proceed"]
                ];
                $j++;
            }
            return $this->admin_report($table);
        }

        return View::make("admin.offers")->withRes($res)->withMode($mode)->withPage($page)->withCount($offers_count)->withList(true)->withCats($cats);
    }
    
    function admin_report($table) {
        // Connected class to work with excel
        require_once(public_path().'/PHPExcel/Classes/PHPExcel.php');
        // Connected class to output data in excel format
        require_once(public_path().'/PHPExcel/Classes/PHPExcel/Writer/Excel5.php');

        // Create object of class PHPExcel
        $xls = new PHPExcel();
        // Install index of active sheet
        $xls->setActiveSheetIndex(0);
        // Receiving active sheet
        $sheet = $xls->getActiveSheet();
        // Sigin sheet
        $sheet->setTitle("Офферы");

        $sheet->setCellValue("B1", 'ТРАФИК');
        $sheet->setCellValue("D1", 'КОЭФФИЦИЕНТЫ');
        $sheet->setCellValue("F1", 'КОНВЕРСИИ');
        $sheet->setCellValue("J1", 'ФИНАНСЫ');
        $sheet->setCellValue("A2", 'ОФФЕР');
        $sheet->setCellValue("B2", 'КЛИКИ');
        $sheet->setCellValue("C2", 'УНИКИ');
        $sheet->setCellValue("D2", 'CR%');
        $sheet->setCellValue("E2", 'EPC');
        $sheet->setCellValue("F2", 'ВСЕГО');
        $sheet->setCellValue("G2", 'ПРИНЯТО');
        $sheet->setCellValue("H2", 'ОЖИДАЕТ');
        $sheet->setCellValue("I2", 'ОТКЛОНЕНО');
        $sheet->setCellValue("J2", 'ВСЕГО');
        $sheet->setCellValue("K2", 'ЗАЧИСЛЕНО');
        $sheet->setCellValue("L2", 'ОЖИДАЕТ');

        $sheet->getStyle("A1:L1")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('58585a');
        $sheet->getStyle("A1:L1")->getFont()->getColor()->setRGB('FFFFFF');

        $sheet->getStyle("A1:L1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle("A2:L2")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getRowDimension(1)->setRowHeight(25);

        foreach ($table as $cell) {
            $sheet->setCellValueByColumnAndRow(
                    $cell["cell_x"], $cell["cell_y"], $cell["value"]);
        }

        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        
        // Output HTTP-headers
        header("Expires: Mon, 1 Apr 1974 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=export.xls");

        // Output file contents
        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }

    //blocking and activation offer
    public function offer_state($id) {
        $offer = Offer::find($id);
        if (!$offer)
            return View::make("new_inner.not_found");
        if ($offer->is_blocked)
            $offer->is_blocked = 0;
        else
            $offer->is_blocked = 1;
        $offer->save();
        return Redirect::back();
    }

    //adding offer
    public function edit_offer($id = 0, $advertiser_id = 0) {
        $cats = Category::all();
        if ($id == 0) {
            $traffic = array(
                array("status" => 1, "name" => "Контекстная реклама"),
                array("status" => 1, "name" => "Popup-формат"),
                array("status" => 1, "name" => "Doorway-трафик"),
                array("status" => 1, "name" => "Мобильный трафик"),
                array("status" => 1, "name" => "AdSpot/RichMedia/Sliding"),
                array("status" => 1, "name" => "Тизерная реклама"),
                array("status" => 1, "name" => "Cashback-трафик"),
                array("status" => 1, "name" => "Таргетированная реклама в соц сетях"),
                array("status" => 1, "name" => "Скидочные и купонные агрегаторы"),
                array("status" => 1, "name" => "Adult-трафик"),
                array("status" => 1, "name" => "Контекстная реклама на бренд"),
                array("status" => 1, "name" => "Clickunder/Popunder"),
                array("status" => 1, "name" => "E-mail рассылка"),
                array("status" => 1, "name" => "Мотивированный трафик"),
                array("status" => 1, "name" => "Брокерский трафик"),
                array("status" => 1, "name" => "Баннерная реклама"),
                array("status" => 1, "name" => "Группы в соц сетях"),
                array("status" => 1, "name" => "Приложения в соц сетях"),
                array("status" => 1, "name" => "Использование промокодов"),
            );
            return View::make("admin.offers", array(
                        "add" => true,
                        "list" => false,
                        "traffic" => $traffic,
                        "cats" => $cats,
                        "advertiser_id" => $advertiser_id,
                        "item" => new Offer,
                        "landings" => array(array("name" => "", "url" => "", "landing" => array())),
                        "aims" => array(array("name" => "", "hold" => "", "postclick" => "", "price" => "")),
            ));
        } else {
            $item = Offer::find($id);
            if (!$item)
                return View::make("new_inner.not_found");
            $traffic = unserialize($item->trafficTypes);
            return View::make("admin.offers", array(
                        "add" => false,
                        "list" => false,
                        "traffic" => $traffic,
                        "item" => $item,
                        "cats" => $cats,
                        "landings" => unserialize($item->landings),
                        "aims" => unserialize($item->aims),
            ));
        }
    }

    public function post_edit_offer($id = 0, $advertiser_id = 0) {
        $valid = array(
            "name" => "required|max:255",
            "description" => "required",
            "link" => "required|max:255",
            "createDate" => "required|date_format:d.m.Y",
            "deeplink" => "max:255",
            "cat" => "required",
        );
        if ($id == 0)
            $valid["logo"] = "required";
        $data = Input::all();
        $validation = Validator::make($data, $valid);
        if ($validation->fails())
            return Redirect::back()->withInput()->withErrors($validation);

        $data = Input::all();
        unset($data["_token"]);

        //landing and deeplink
        $links = array();
        foreach ($data["landing"] as $key => $land) {
            $links[] = array(
                "id" => $key + 1,
                "name" => $data["landing_name"][$key],
                "url" => $land,
                "landing" => array(
                    "id" => $key + 1,
                ),
            );
        }
        if ($data["deeplink"] != "") {
            $links[] = array(
                "id" => 99999999,
                "name" => "deeplink",
                "url" => $data["deeplink"],
                "landing" => "",
            );
        }
        $data["landings"] = serialize($links);
        unset($data["landing"]);
        unset($data["landing_name"]);
        unset($data["deeplink"]);

        //logo
        if (Input::hasFile('logo')) {
            $destinationPath = public_path() . '/upload_logos';
            $filename = str_random(12) . "_" . Input::file('logo')->getClientOriginalName();
            if (Input::file('logo')->move($destinationPath, $filename)) {
                $data["logo"] = asset("/upload_logos/" . $filename);
            } else {
                return Redirect::back()->withAlert("Не удалось загрузить файл")->withInput();
            }
        } else {
            unset($data["logo"]);
        }

        //Goals
        $aims = array();
        foreach ($data["aim_name"] as $key => $aim) {
            $aims[] = array(
                "id" => $key + 1,
                "name" => $aim,
                "hold" => $data["aim_hold"][$key],
                "postclick" => $data["aim_postclick"][$key],
                "price" => $data["aim_price"][$key],
            );
        }
        $data["aims"] = serialize($aims);
        unset($data["aim_name"]);
        unset($data["aim_hold"]);
        unset($data["aim_postclick"]);
        unset($data["aim_price"]);

        //Traffic
        $traf = array();
        foreach ($data["traffic_name"] as $key => $traffic) {
            $traf[] = array(
                "name" => $traffic,
                "status" => isset($data["traffic"][$key]) ? 3 : 1,
            );
        }
        $data["trafficTypes"] = serialize($traf);
        unset($data["traffic"]);
        unset($data["traffic_name"]);

        //Categories
        $cats = array();
        foreach ($data["cat"] as $cat_id => $value) {
            $cats[] = $cat_id;
        }
        unset($data["cat"]);

        if ($id == 0) {
            $item = Offer::create($data);
        } else {
            $item = Offer::find($id);
            $item->update($data);
        }
        //Link to category
        $item->categories()->sync($cats);
        //Link to user
        if ($id == 0)
            $item->users()->sync(array($advertiser_id));
        return Redirect::to("/admin/offers");
    }

    public function getDelete($id) {
        $item = Offer::find($id);
        if (!$item)
            return View::make("new_inner.not_found");
        return View::make("admin.delete")->withType("оффер")->withName($item->name);
    }

    public function postDelete($id) {
        if (Input::has("delete")) {
            $item = Offer::find($id);
            $item->categories()->sync(array());
            Click::where("offer_id", $id)->delete();
            $item->users()->sync(array());
            Stat::where("offer_id", $id)->delete();
            Stream::where("offer_id", $id)->delete();
            $item->delete();
        }
        return Redirect::to("/admin/offers");
    }

    //Level
    public function levelup($id) {
        $offer = Offer::find($id);
        if (!$offer)
            return View::make("new_inner.not_found");
        if ($offer->levelup)
            $offer->levelup = false;
        else
            $offer->levelup = true;
        $offer->save();
        return Redirect::back();
    }

    public function lids_scripts($id) {
        $offer = Offer::find($id);
        if (!$offer)
            return View::make("new_inner.not_found");
        $aims = unserialize($offer->aims);
        return View::make("admin.lids_scripts", array(
                    "aims" => $aims,
                    "offer" => $offer,
        ));
    }

}
